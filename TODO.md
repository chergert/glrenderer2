
 * Saving uniform state when serializing to a command batch
 * Ability to merge multiple batches together
   - `gsk_gl_command_batch_merge(GskGLCommandBatch *self, GskGLCommandBatch *other)`
 * Track clip/modelview/projection/etc in command batch
 * If we store the transformed bounds in batches, then we can more easily look
   for overlaps to determine merge compatibility
 * If we use some huffman coding to track how often items within our atlases are
   used, we could help reduce how often we change textures by getting them onto
   the same texture. Think of it as a sort of GC for texture data. Since the data
   is already uploaded, it's fast to move them onto a new texture from different
   sources.
 * Implement `gsk_gl_command_batch_run()` to apply changes

For the state tracking, we need a way to apply state changes as we go to
determine if batches can be merged. Since we currently only store the
state as it's applied, we need a way to snapshot that into the batch. But also,
we need to be able to determine the whole set of uniforms at any batch to
compare them with others. Otherwise, we cannot merge out-of-order draws.

It might make sense to allocate space for each additional uniform value change
at the end of the array so that we can simply point at the static values from
the diff/change/tracker objects. That also makes things more compact in memory
from the diff standpoint. It would also be nice to have a compressed form at the
end of diff to be the initial state for next frame.

Another idea could be for the uniform state to have a "current batch" associated
with it so that changing uniforms will place the change into the batch directly
so that we don't need to copy it later.

What we really need though, is the ability to see the whole uniform state
at any time for a batch so that we can compare. Having a shadow uniform state
which can collect changes as we go (with pointers into that batches local
memory zone for the uniform) might be able to provide this so long as we dont
delete batch nodes until the queue is flushed.

It might also be nice for Batch to have a link node in it for sorting.

We might also want an index based on program types to jump ahead to similar
draws to potentially draw out of order.

