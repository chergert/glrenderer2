typedef struct _GdkGLContext GdkGLContext;

#define GSK_NOTE(...)

static inline void
gdk_gl_context_push_debug_group_printf (GdkGLContext *context,
                                        const char   *format,
                                        ...)
{
}

static inline void
gdk_gl_context_pop_debug_group (GdkGLContext *context)
{
}
