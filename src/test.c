#include <gsk/gsk.h>
#include <gtk/gtk.h>
#include <stdlib.h>

#include "gsk-gl-command-queue-private.h"
#include "gsk-gl-renderer-private.h"

int
main (int argc,
      char *argv[])
{
  const char *filename;
  GdkSurface *surface;
  GtkWidget *window;
  GError *error = NULL;
  GBytes *bytes;
  GFile *file;
  GskRenderNode *root;
  GskNextRenderer *renderer;
  graphene_rect_t viewport;
  GdkTexture *texture;

  if (argc < 2)
    {
      g_printerr ("%s FILENAME\n", argv[0]);
      return EXIT_FAILURE;
    }

  filename = argv[1];
  file = g_file_new_for_commandline_arg (filename);

  if (!(bytes = g_file_load_bytes (file, NULL, NULL, &error)))
    {
      g_printerr ("Failed to load %s: %s\n", filename, error->message);
      return EXIT_FAILURE;
    }

  gtk_init ();

  if (!(root = gsk_render_node_deserialize (bytes, NULL, NULL)))
    return EXIT_FAILURE;

  window = gtk_window_new ();
  gtk_window_set_default_size (GTK_WINDOW (window), 640, 480);
  gtk_widget_realize (window);

  surface = gtk_native_get_surface (GTK_NATIVE (window));
  renderer = gsk_next_renderer_new ();

  gsk_renderer_realize (GSK_RENDERER (renderer), surface, &error);
  g_assert_no_error (error);

  viewport.origin.x = 0;
  viewport.origin.y = 0;
  viewport.size.width = 800;
  viewport.size.height = 600;

  texture = gsk_renderer_render_texture (GSK_RENDERER (renderer), root, &viewport);

  g_print ("Texture: %p\n", texture);

  gsk_renderer_unrealize (GSK_RENDERER (renderer));
  g_clear_object (&renderer);
  gtk_window_destroy (GTK_WINDOW (window));

  return 0;
}
