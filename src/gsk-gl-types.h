/* gsk-gl-types.h
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#ifndef GSK_GL_TYPES_H
#define GSK_GL_TYPES_H

#include <graphene.h>
#include <epoxy/gl.h>
#include <gdk/gdk.h>
#include <gsk/gsk.h>

typedef struct _GskGLAttachmentState GskGLAttachmentState;
typedef struct _GskGLCommandQueue GskGLCommandQueue;
typedef struct _GskGLCompiler GskGLCompiler;
typedef struct _GskGLDrawVertex GskGLDrawVertex;
typedef struct _GskGLGlyphLibrary GskGLGlyphLibrary;
typedef struct _GskGLIconLibrary GskGLIconLibrary;
typedef struct _GskGLProgram GskGLProgram;
typedef struct _GskGLShadowLibrary GskGLShadowLibrary;
typedef struct _GskGLTextureAtlas GskGLTextureAtlas;
typedef struct _GskGLTextureLibrary GskGLTextureLibrary;
typedef struct _GskGLUniformState GskGLUniformState;
typedef struct _GskNextDriver GskNextDriver;
typedef struct _GskGLRenderJob GskGLRenderJob;

struct _GskGLDrawVertex
{
  float position[2];
  float uv[2];
};

#define GSK_DEBUG_CHECK(d) 0

static inline gboolean
gsk_rounded_rect_equal (const GskRoundedRect *a,
                        const GskRoundedRect *b)
{
  return graphene_rect_equal (&a->bounds, &b->bounds) &&
         graphene_size_equal (&a->corner[0], &b->corner[0]) &&
         graphene_size_equal (&a->corner[1], &b->corner[1]) &&
         graphene_size_equal (&a->corner[2], &b->corner[2]) &&
         graphene_size_equal (&a->corner[3], &b->corner[3]);
}

struct _GskRendererClass
{
  GObjectClass parent_class;

  gboolean             (* realize)                              (GskRenderer            *renderer,
                                                                 GdkSurface             *surface,
                                                                 GError                **error);
  void                 (* unrealize)                            (GskRenderer            *renderer);

  GdkTexture *         (* render_texture)                       (GskRenderer            *renderer,
                                                                 GskRenderNode          *root,
                                                                 const graphene_rect_t  *viewport);
  void                 (* render)                               (GskRenderer            *renderer,
                                                                 GskRenderNode          *root,
                                                                 const cairo_region_t   *invalid);
};

struct _GskRenderer
{
  GObject parent_instance;
};

#define GSK_RENDERER_CLASS(c) ((gpointer)c)

#endif /* GSK_GL_TYPES_H */
